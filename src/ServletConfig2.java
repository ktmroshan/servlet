import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ServletConfig2", urlPatterns = { "/hello" }, initParams = {
		@WebInitParam(name = "Site :", value = "http://roseindia.net"),
		@WebInitParam(name = "Rose", value = "India"), })
public class ServletConfig2 extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h2>Init Param Servlet Example</h2>");
		ServletConfig config = getServletConfig();
		String pValue = config.getInitParameter("Site :");
		out.println("Param Value : " + pValue);
		String pValue1 = config.getInitParameter("Rose");
		out.println("<br>Param Value : " + pValue1);
		out.close();
	}

}