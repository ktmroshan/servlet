import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

// Creating Http Servlet by Extending HttpServlet class
public class ExampleHttpServlet extends HttpServlet {
	private String mymsg;

	public void init() throws ServletException {
		mymsg = "Http Servlet Demo";
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		if (session == null) {
			System.out.println("-- creating new session in the servlet --");
			session = request.getSession(true);
			System.out.println("-- session created in the servlet --");
		}

		session.setAttribute("test-attribute-11", "test attribute value 1");
		session.setAttribute("test-attribute-11", "test attribute value 1");
		session.removeAttribute("test-attribute-11");

		response.setContentType("text/html");
		PrintWriter w = response.getWriter();
		w.write("Hello !!");

	}

	public void destroy() {
		// Leaving empty. Use this if you want to perform
		// something at the end of Servlet life cycle.
	}
}