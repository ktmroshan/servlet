import java.io.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/RequestDipacherTest")
public class RequestDipacherTest extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter pwriter = response.getWriter();
		String requestType = request.getParameter("requestType");
		if (requestType.equals("forward")) {
			RequestDispatcher dis = request.getRequestDispatcher("index.html");
			dis.forward(request, response);
			pwriter.write("Hello Test");
		} else {
			
			RequestDispatcher dis = request.getRequestDispatcher("index.html");
			dis.include(request, response);
			pwriter.write("Hello Test");
		}
	}
}